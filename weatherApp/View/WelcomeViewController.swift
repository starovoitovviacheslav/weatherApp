//
//  WelcomeViewController.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 18.06.2021.
//

import UIKit
import CoreLocation

class WelcomeViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var buttonCTA: UIButton!
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.hideKeyboardWhenTappedAround()
        
        textfieldCityName = nil
        
        //MARK: - Properties

        startLabel.title("Type your city and check out the weather")
        subLabel.semititle("You can select any later")
        buttonCTA.mainButton(forNormalTitle: "Check weather",
                             forDisableTitle: "Please, type some city")
        
        textField.textFieldProperty()
        let searchImage = UIImage(named: "searchIcon")
        textField.setSearchIcon(searchImage!)
        textField.delegate = self
        
        textField.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        
        let button = UIButton(type: .custom)
        textField.setNavButton(button: button)
        button.addTarget(self, action: #selector(getDataFromGeo), for: .touchUpInside)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        buttonCTA.isEnabled = false
        buttonCTA.setTitle("Please, type some city", for: .disabled)
        buttonCTA.alpha = 0.5
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        textField.text = ""
        self.view.deleteBlurAndSpinner()
    }
    
    //MARK: - Functions
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let first = locations.first else {
            return
        }
        currentLon = Double(first.coordinate.longitude)
        currentLat = Double(first.coordinate.latitude)
        print("Latitude:\(first.coordinate.latitude), Longitude:\(first.coordinate.longitude)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(first) { (placemarks, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }
            if let placemarks = placemarks {
                if placemarks.count > 0 {
                    let placemark = placemarks[0]
                    if let city = placemark.locality {
                        rootCityName = city
                        print("City is: \(city)")
                    }
                }
            }
        }
        
    }
    
    //Parse by navigation icon
    
    @objc func getDataFromGeo() {

        self.view.addBlurAndSpinner()
        
        viewModel.deleteData()
        
        getDataWithLatLon {
            NetworkManager.shared.getUnsplashPhoto(cityName: rootCityName ?? viewModel.weather.value.current.weather[0].main) { url in
                stringPicURL = url
                DispatchQueue.main.async {
                    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "MainVC") as! MainViewController
                    self.navigationController!.pushViewController(nextVC, animated: true)
                    print("Data after all methods: \(viewModel.weather.value)")
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        if textField.text?.count == 1 {
            if textField.text?.first == " " {
                textField.text = ""
                return
            }
        }
        guard let text = textField.text, !text.isEmpty else {
            buttonCTA.isEnabled = false
            return
        }
        buttonCTA.alpha = 1
        buttonCTA.isEnabled = true
    }
    
//MARK: - IBAction
    
    @IBAction func buttonCTA(_ sender: Any) {
        
        viewModel.deleteData()
        
        guard let city = self.textField.text?.removingWhitespaces() else {
            self.presentAlertController(title: "Wrong city name", message: "Please, type another one")
            return
        }
        self.view.addBlurAndSpinner()
        
//        let city = text.split(separator: " ").joined(separator: "%20")
        
        getDataWithCityName(cityName: city) {
            NetworkManager.shared.getUnsplashPhoto(cityName: city) { url in
                stringPicURL = url
                DispatchQueue.main.async {
                    textfieldCityName = city
                    let nextVC = self.storyboard!.instantiateViewController(withIdentifier: "MainVC") as! MainViewController
                    self.navigationController!.pushViewController(nextVC, animated: true)
                }
            }
        } alertError: {
            self.presentAlertController(title: "Wrong city name", message: "Please, type another one")
        }

    }


}

extension WelcomeViewController {
    func presentAlertController(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
        
        self.textField.text?.removeAll()
        self.view.deleteBlurAndSpinner()
    }
}
