//
//  MainViewController.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 18.06.2021.
//

import UIKit
import CoreLocation

class MainViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var cityPicBackground: UIImageView!
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var picWeather: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
   

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var viewTab: UIView!
    
    
    @IBOutlet var firstDateLabel: UILabel!
    @IBOutlet var firstIconPic: UIImageView!
    @IBOutlet var firstTempLabel: UILabel!
    
    @IBOutlet var secondDateLabel: UILabel!
    @IBOutlet var secondIconPic: UIImageView!
    @IBOutlet var secondTempLabel: UILabel!
    
    @IBOutlet var thirdDateLabel: UILabel!
    @IBOutlet var thirdIconPic: UIImageView!
    @IBOutlet var thirdTempLabel: UILabel!
    
    @IBOutlet var fourthDateLabel: UILabel!
    @IBOutlet var fourthIconPic: UIImageView!
    @IBOutlet var fourthTempLabel: UILabel!
    
    
    @IBOutlet var fifthDateLabel: UILabel!
    @IBOutlet var fifthIconPic: UIImageView!
    @IBOutlet var fifthTempLabel: UILabel!
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: - Data parsing

        getCurrentWeather()
        cityPicBackground.load(urlString: stringPicURL)
        
        //MARK: - Properties
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"navIcon"), style: .plain, target: self, action: #selector(refreshWeather))
        
        let dateLabels = [firstDateLabel, secondDateLabel, thirdDateLabel, fourthDateLabel, fifthDateLabel]
        
        let tempLabels = [firstTempLabel, secondTempLabel, thirdTempLabel, fourthTempLabel, fifthTempLabel]
        
        cityName.font = UIFont.init(name: "Mont-Bold", size: 32)
        cityName.textColor = UIColor.init(named: "objectColor")
        cityName.textAlignment = .center
        
        picWeather.tintColor = UIColor(named: "objectColor")
        
        tempLabel.font = UIFont.init(name: "Mont-Bold", size: 32)
        tempLabel.textColor = UIColor.init(named: "objectColor")
        
        viewTab.layer.cornerRadius = 18
        viewTab.layer.backgroundColor = UIColor(named: "gradientColor")?.cgColor
        
        segmentedControl.titles()
        segmentedControl.custom()
        
        segmentedControl.selectedSegmentIndex = 0
        
        for temp in tempLabels {
            temp?.textAlignment = .center
            temp?.textColor = UIColor(named: "objectColor")
            temp?.font = UIFont.init(name: "Mont-SemiBold", size: 15)
        }
        
        for date in dateLabels {
            date?.textAlignment = .center
            date?.textColor = UIColor(named: "objectColor")
            date?.font = UIFont.init(name: "Mont-SemiBold", size: 15)
        }
        
    }
    
    
    //MARK: - Functions
    
    func getCurrentWeather() {
        
        cityName.text = {
            if textfieldCityName != nil {
                return textfieldCityName!
            } else {
                return rootCityName
            }
        }()
        
        
        tempLabel.text = {
            if Int(viewModel.weather.value.current.temp) > 0 {
                return "+" + String(Int(viewModel.weather.value.current.temp)) + "ºC"
            } else {
                return String(Int(viewModel.weather.value.current.temp)) + "ºC"
            }
        }()
        
        picWeather.image = UIImage(named: iconConvert(icon: viewModel.weather.value.current.weather[0].icon))
        
        let dateLabels = [firstDateLabel, secondDateLabel, thirdDateLabel, fourthDateLabel, fifthDateLabel]
        let iconPics = [firstIconPic, secondIconPic, thirdIconPic, fourthIconPic, fifthIconPic]
        let tempLabels = [firstTempLabel, secondTempLabel, thirdTempLabel, fourthTempLabel, fifthTempLabel]
        
        parseHourlyWeather(dateLabels: dateLabels, iconPics: iconPics, tempLabels: tempLabels)
        
    }
    
    func segmentedControlData(sender: UISegmentedControl) {
        
        let dateLabels = [firstDateLabel, secondDateLabel, thirdDateLabel, fourthDateLabel, fifthDateLabel]
        let iconPics = [firstIconPic, secondIconPic, thirdIconPic, fourthIconPic, fifthIconPic]
        let tempLabels = [firstTempLabel, secondTempLabel, thirdTempLabel, fourthTempLabel, fifthTempLabel]
        
        if sender.selectedSegmentIndex == 1 {
            parseDailyWeather(dateLabels: dateLabels, iconPics: iconPics, tempLabels: tempLabels)
        } else {
            parseHourlyWeather(dateLabels: dateLabels, iconPics: iconPics, tempLabels: tempLabels)
        }
        
    }
    
    
    @IBAction func segmentedContrAction(_ sender: UISegmentedControl) {
        segmentedControlData(sender: sender)
    }
    
    @objc func refreshWeather(_ sender : UIBarButtonItem) {
        
        self.view.addBlurAndSpinner()
        
        sender.tintColor = .systemOrange
        
        getDataWithLatLon {
            
            DispatchQueue.main.async {
                NetworkManager.shared.getUnsplashPhoto(cityName: rootCityName ?? viewModel.weather.value.current.weather[0].main) { url in
                    stringPicURL = url
                    self.cityPicBackground.load(urlString: stringPicURL)
                }
                refreshData(cityName: self.cityName, tempLabel: self.tempLabel, picWeather: self.picWeather)

                
                self.segmentedControlData(sender: self.segmentedControl)
                
                self.view.deleteBlurAndSpinner()

            }
        }
    }

    
}


