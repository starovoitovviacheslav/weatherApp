//
//  Picture.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 29.06.2021.
//

import Foundation

struct UnspashAPI: Codable {
    let total: Int
    let total_pages: Int
    let results: [Results]
}

struct Results: Codable {
    let id: String
    let urls: URLS
}

struct URLS: Codable {
    let regular: String
}
