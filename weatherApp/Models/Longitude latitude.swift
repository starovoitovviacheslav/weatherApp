//
//  Longitude latitude.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 02.07.2021.
//

import Foundation

struct OnlyLonAndLat: Codable {
    let coord: Coord
}

struct Coord: Codable {
    let lon: Double
    let lat: Double
}
