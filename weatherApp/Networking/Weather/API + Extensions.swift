//
//  API + Extensions.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 14.06.2021.
//

import Foundation

extension weatherAPI {
    static let weatherURLString = "https://api.openweathermap.org/data/2.5/"
    
    static func getURLFor(lat: Double, lon: Double) -> String {
        return "\(weatherURLString)onecall?lat=\(lat)&lon=\(lon)&exclude=minutely&appid=\(key)&units=metric"
    }
    
}
