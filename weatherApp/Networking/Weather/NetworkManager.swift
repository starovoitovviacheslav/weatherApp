//
//  NetworkManager.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 14.06.2021.
//

import Foundation

class NetworkManager {
    
    static var shared = NetworkManager()
    
    func parseByCityName(cityName: String, completion: @escaping (OnlyLonAndLat) -> Void, completionError: @escaping () -> Void) {
        
        let stringURL = "https://api.openweathermap.org/data/2.5/weather?q=\(cityName)&appid=\(weatherAPI.key)"
        guard let URL = URL(string: stringURL) else {
            print("can't convert string AD into url")
            DispatchQueue.main.async {
                completionError()
            }
            return
        }
        
        print(URL)
        
        URLSession.shared.dataTask(with: URL) { data, response, error in
            guard let data = data else {
                print("data not found")
                return
            }
            
            print("Count of lan lot data: \(data)")
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                print("response error")
                DispatchQueue.main.async {
                    completionError()
                }
                return
            }
            
            if error != nil {
                print(error?.localizedDescription ?? "error without localizedDescription")
            }
            
            do {
                
                let json = try? JSONDecoder().decode(OnlyLonAndLat.self, from: data)
                if let json = json {
                    print(json)
                    completion(json)
                }
                
            } catch {
               
            }
        }.resume()
        
    }
    
    func rootParse(latitude: Double, lontitude: Double, completionSuccess: @escaping (Result) -> Void, completionError: @escaping (String) -> Void) {
                
        let stringUrl = weatherAPI.getURLFor(lat: latitude, lon: lontitude)
        
        guard let url = URL(string: stringUrl) else {
            print("can't convert string to url")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
                
                guard let data = data else {
                    print("data not found")
                    return
                }
                
                print(data)
                
                guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                    print("response error")
                    
                    return
                }
                
                if error != nil {
                    print(error?.localizedDescription ?? "error without localizedDescription")
                }
                
                do {
                    
                    let weather = try? JSONDecoder().decode(Result.self, from: data)
                    guard let weather = weather else {
                        print("empty json")
                        return
                    }
                    completionSuccess(weather)
                    
                } catch {
                    completionError(error.localizedDescription)
                }
            
            
        }.resume()
    }
    
    func getUnsplashPhoto(cityName: String, completionHandler: @escaping (String) -> Void) {
        
        let unspashAPIKey = "AE_8_B2v_iISOr1dDJ-H23eOshxJKd2AVuEfhUE-WRg"

        let string = "https://api.unsplash.com/search/photos?page=1&query=\(cityName)&client_id=\(unspashAPIKey)"
        
        guard let url = URL(string: string) else {
            print("can't convert url")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                print("data not found")
                return
            }
            
            print(data)
            
            if let response = response {
                print("Response is: \(response)")
            }
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            
            do {
                
                let jsonResult = try JSONDecoder().decode(UnspashAPI.self, from: data)
                print(jsonResult.results[0].urls.regular)
                completionHandler(jsonResult.results[0].urls.regular)
                
            } catch {
                print(error.localizedDescription)
            }
            
        }.resume()
        
        
    }

}

