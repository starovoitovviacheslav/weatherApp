//
//  Extensions.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 15.09.2021.
//

import UIKit

//MARK: - View

extension UIView {
    
    func addBlurAndSpinner() {
        
        //blur
        let imageView = UIImageView(image: UIImage(named: "example"))
        imageView.frame = self.bounds
        imageView.contentMode = .scaleToFill
        self.addSubview(imageView)

        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = imageView.bounds
        self.addSubview(blurredEffectView)
        
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
        vibrancyEffectView.frame = imageView.bounds
        
        blurredEffectView.tag = 9
        
        //spinner
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.color = .white
        spinner.center = self.center
        spinner.startAnimating()
        self.addSubview(spinner)
        
        spinner.tag = 8
    }
    
    func deleteBlurAndSpinner() {
        DispatchQueue.main.async {
            self.viewWithTag(8)?.removeFromSuperview()
            self.viewWithTag(9)?.removeFromSuperview()
        }
    }
}

//MARK: - ImageView

extension UIImageView {
    func load(urlString: String) {
        guard let url = URL(string: urlString) else {
            print("can't convert city picture URL")
            return
        }
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

//MARK: - ViewController

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

//MARK: - Image

extension UIImage{

    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return rectangleImage!
    }
}

//MARK: - SegmentedControl

extension UISegmentedControl{
    
    func titles() {
        self.setTitle("Today", forSegmentAt: 0)
        self.setTitle("Week", forSegmentAt: 1)
    }
    
    func custom(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor(named: "gradientColor")!.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        
        
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor(named: "gradientColor")!.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        
        self.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.init(name: "Mont-SemiBold", size: 14) as Any, NSAttributedString.Key.foregroundColor: UIColor(named: "unselected")  as Any], for: .normal)
  
        self.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.init(name: "Mont-Bold", size: 16) as Any, NSAttributedString.Key.foregroundColor: UIColor(named: "objectColor")  as Any], for: .selected)
        
        
    }

}

//MARK: - TextField

extension UITextField {
    
    func textFieldProperty() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.cornerRadius = 15
        
        self.returnKeyType = .continue
        self.textAlignment = .left
        self.contentVerticalAlignment = .center
        self.tintColor = UIColor(named: "mainColor")
        self.font = UIFont.init(name: "Mont-Bold", size: 18)
        self.layer.masksToBounds = false
        self.textColor = .black
        self.attributedPlaceholder = NSAttributedString(string: "My city is...", attributes: [NSAttributedString.Key.foregroundColor: UIColor.systemGray])
        
    }
    
    func setSearchIcon(_ image: UIImage) {
       let iconView = UIImageView(frame:
                      CGRect(x: 12, y: 3, width: 24, height: 24))
       iconView.image = image
       let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 30, y: 0, width: 50, height: 30))
       iconContainerView.addSubview(iconView)
       leftView = iconContainerView
       leftViewMode = .always
    }
    
    func setNavigationIcon(_ image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 12, y: 3, width: 24, height: 24))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
                       CGRect(x: 30, y: 0, width: 50, height: 30))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
    
    func setNavButton(button: UIButton) {
 
        
        button.setImage(UIImage(named: "navIcon"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25),
                              y: CGFloat(5),
                              width: CGFloat(25),
                              height: CGFloat(25))

        self.rightView = button
        self.rightViewMode = .always
        
        
    }
    
}

//MARK: - String

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined(separator: "%20")
    }
}

//MARK: - Label

extension UILabel {
    func title(_ text: String) {
        self.font = UIFont.init(name: "Mont-Bold", size: 32)
        self.textColor = UIColor.white
        self.textAlignment = .center
        self.text = text
        self.lineBreakMode = .byClipping
        self.numberOfLines = 3
    }
    func semititle(_ text: String) {
        self.font = UIFont.init(name: "Mont-SemiBold", size: 15)
        self.textColor = UIColor.white
        self.textAlignment = .center
        self.text = text
    }
}

//MARK: - Button

extension UIButton {
    func mainButton(forNormalTitle: String, forDisableTitle: String) {
        self.isEnabled = false
        self.layer.cornerRadius = 20
        self.layer.backgroundColor = UIColor.white.cgColor
        self.titleLabel?.font = UIFont.init(name: "Mont-Bold", size: 18)
        self.titleLabel?.textColor = UIColor(named: "mainColor")
        self.setTitle(forNormalTitle, for: .normal)
        self.setTitle(forDisableTitle, for: .disabled)
        if !self.isEnabled {
            self.alpha = 0.5
        } else {
            self.alpha = 1
        }
    }
}
