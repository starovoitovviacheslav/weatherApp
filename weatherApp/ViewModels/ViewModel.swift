//
//  CityGetter.swift
//  weatherApp
//
//  Created by Slava Starovoitov on 14.06.2021.
//

import Foundation
import UIKit
import CoreLocation

class Observable<T> {

    var value: T {
        didSet {
            listener?(value)
        }
    }

    private var listener: ((T) -> Void)?

    init(_ value: T) {
        self.value = value
    }

    func bind(_ closure: @escaping (T) -> Void) {
        closure(value)
        self.listener = closure
    }
}


struct WeatherListViewModel {
    var weather: Observable<Result> = Observable(Result.empty())
    mutating func deleteData() {
        self.weather = Observable(Result.empty())
    }
}

var viewModel = WeatherListViewModel()



//MARK: - Converts
func dailyConvert(dt: Double) -> String {
    let date = NSDate(timeIntervalSince1970: dt)
    let dateFormatter = DateFormatter()
    dateFormatter.setLocalizedDateFormatFromTemplate("MMM d")
    return String(dateFormatter.string(from: date as Date))
}

func hourlyConvert(dt: Double) -> String {
    let date = NSDate(timeIntervalSince1970: dt)
    let dateFormatter = DateFormatter()
    dateFormatter.setLocalizedDateFormatFromTemplate("HH:mm")
    return String(dateFormatter.string(from: date as Date))
}

func iconConvert(icon: String) -> String {
    switch icon {
        case "01d":
            return "clearSkyD"
        case "01n":
            return "clearSkyN"
        case "02d":
            return "fewCloudsD"
        case "02n":
            return "fewCloudsN"
        case "03d", "03n":
            return "scatteredClouds"
        case "04d", "04n":
            return "brokenClouds"
        case "09d", "09n":
            return "showerRain"
        case "10d":
            return "rainD"
        case "10n":
            return "rainN"
        case "11d", "11n":
            return "thunder"
        case "13d", "13n":
            return "snow"
        case "50d", "50n":
            return "mist"
        default:
            return "clearSkyD"
        }
}

//MARK: - Properties

public var lat: Double = 0.0
public var lon: Double = 0.0

public var currentLat: Double = 0.0
public var currentLon: Double = 0.0

public var rootCityName: String? = nil
public var textfieldCityName: String? = nil

var stringPicURL = ""

//MARK: - Functions

func geoCoder() {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let first = locations.first else {
            return
        }
        currentLon = Double(first.coordinate.longitude)
        currentLat = Double(first.coordinate.latitude)
        print("Latitude:\(first.coordinate.latitude), Longitude:\(first.coordinate.longitude)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(first) { (placemarks, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }
            if let placemarks = placemarks {
                if placemarks.count > 0 {
                    let placemark = placemarks[0]
                    if let city = placemark.locality {
                        rootCityName = city
                        print("City is: \(city)")
                    }
                }
            }
        }
        
    }
}

//------------------------------------------------------------------

func getDataWithLatLon(completion: @escaping () -> Void) {

        NetworkManager.shared.rootParse(latitude: currentLat, lontitude: currentLon) { (result) in
            viewModel.weather.value = result
            completion()
        } completionError: { error in
            debugPrint(error)
        }
    
}

func getDataWithCityName(cityName: String, callback: @escaping () -> Void, alertError: @escaping () -> Void) {
    NetworkManager.shared.parseByCityName(cityName: cityName) { coordiantes in
        NetworkManager.shared.rootParse(latitude: coordiantes.coord.lat, lontitude: coordiantes.coord.lon) { result in
            viewModel.weather.value = result
            print("DATA BY CITY NAME: \(viewModel.weather.value)")
            callback()
        } completionError: { error in
            debugPrint(error)
        }

    } completionError: {
        alertError()
    }

}


func refreshData(cityName: UILabel, tempLabel: UILabel, picWeather: UIImageView) {

    cityName.text = rootCityName
    tempLabel.text = {
        if Int(viewModel.weather.value.current.temp) > 0 {
            return "+" + String(Int(viewModel.weather.value.current.temp)) + "ºC"
        } else {
            return String(Int(viewModel.weather.value.current.temp)) + "ºC"
        }
    }()
    picWeather.image = UIImage(named: iconConvert(icon: viewModel.weather.value.current.weather[0].icon))
    
}

//------------------------------------------------------------------


func parseHourlyWeather(dateLabels: [UILabel?], iconPics: [UIImageView?], tempLabels: [UILabel?]) {
    var (j, k, l) = (-1, -1, -1)
    while j < 4 {

        for icon in iconPics {
            j += 1
            icon?.image = UIImage(named: iconConvert(icon: viewModel.weather.value.hourly[j].weather[0].icon))
        }

        for date in dateLabels {
            k += 1
            date?.text = hourlyConvert(dt: Double(viewModel.weather.value.hourly[k].dt))
        }

        for temp in tempLabels {
            l += 1
            if Int(viewModel.weather.value.hourly[l].temp) > 0 {
                temp?.text = "+" + String(Int(viewModel.weather.value.hourly[l].temp)) + "ºC"
            } else {
                temp?.text = String(Int(viewModel.weather.value.hourly[l].temp)) + "ºC"
            }
        }

    }
}

func parseDailyWeather(dateLabels: [UILabel?], iconPics: [UIImageView?], tempLabels: [UILabel?]) {
    var (j, k, l) = (-1, -1, -1)
    while j < 4 {

        for icon in iconPics {
            j += 1
            icon?.image = UIImage(named: iconConvert(icon: viewModel.weather.value.daily[j].weather[0].icon))
        }

        for date in dateLabels {
            k += 1
            date?.text = dailyConvert(dt: Double(viewModel.weather.value.daily[k].dt))
        }   

        for temp in tempLabels {
            l += 1
            temp?.text = "+" + String(Int(viewModel.weather.value.daily[l].temp.max)) + "ºC"
        }

    }
}

//------------------------------------------------------------------







